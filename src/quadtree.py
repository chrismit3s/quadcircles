import numpy as np


class QuadTree:
    def __init__(self, size, min_size):
        self.node = Leaf((0, 0), size, min_size)

    def traverse(self, func):
        self.node.traverse(func)

    def divide(self, pos):
        self.node, children = self.node.divide(pos)
        return children

    def divide_all(self):
        self.node = self.node.divide_all()

    def get(self, pos):
        return self.node.get(pos)


class Node:
    def topleft(self):
        raise NotImplementedError

    def bottomright(self):
        raise NotImplementedError

    def rect(self):
        tl = self.topleft()
        br = self.bottomright()
        return np.array((tl, br - tl))

    def can_divide(self):
        raise NotImplementedError

    def contains(self, pos):
        return (self.topleft() <= pos).all() and (pos < self.bottomright()).all()

    def traverse(self, _):
        raise NotImplementedError

    def divide(self, _):
        raise NotImplementedError

    def divide_all(self):
        raise NotImplementedError

    def get(self, _):
        raise NotImplementedError


class Tree(Node):
    def __init__(self, leaf):
        hx, hy = half = leaf.size // 2
        cx, cy = leaf.size - half
        px, py = leaf.pos
        nx, ny = leaf.pos + half
        self.children = np.array(
            (
                (
                    Leaf((px, py), (hx, hy), leaf.min_size),
                    Leaf((nx, py), (cx, hy), leaf.min_size),
                ),
                (
                    Leaf((px, ny), (hx, cy), leaf.min_size),
                    Leaf((nx, ny), (cx, cy), leaf.min_size),
                ),
            )
        )
        self.mid = leaf.pos + half
        self._can_divide = True

    def topleft(self):
        return self.children[0, 0].topleft()

    def bottomright(self):
        return self.children[-1, -1].bottomright()

    def can_divide(self):
        self._can_divide = self._can_divide and any(child.can_divide() for child in self.children.flat)
        return self._can_divide

    def traverse(self, func):
        for child in self.children.flat:
            child.traverse(func)

    def divide(self, pos):
        if not self.can_divide():
            return self, None

        i = self.index(pos)
        self.children[i], changed = self.children[i].divide(pos)
        return self, changed

    def divide_all(self):
        for i, child in np.ndenumerate(self.children):
            self.children[i] = child.divide_all()
        return self

    def get(self, pos):
        i = self.index(pos)
        return self.children[i].get(pos)

    def index(self, pos):
        return tuple((pos >= self.mid).astype(int)[::-1])


class Leaf(Node):
    def __init__(self, pos, size, min_size):
        self.pos = np.array(pos, dtype=int)
        self.size = np.array(size, dtype=int)
        self.min_size = np.array(min_size, dtype=int)

    def topleft(self):
        return self.pos

    def bottomright(self):
        return self.pos + self.size

    def can_divide(self):
        return (self.min_size <= self.size / 2).all()

    def traverse(self, func):
        func(self)

    def divide(self, _=None):
        # first arg is for reassignment in the previous node,
        # second arg is the actual return value
        tree = Tree(self)
        return (tree, tree) if self.can_divide() else (self, self)

    def divide_all(self):
        return self.divide()[0]

    def get(self, _):
        return self
