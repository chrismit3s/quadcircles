from argparse import ArgumentParser
from src.quadtree import QuadTree
from os.path import join
import cv2
import numpy as np
import os
import pygame as pg
import pygame.gfxdraw as gfx
import sys


pg.init()


MAX_SIZE = 0.9  # relative to screen size, correction for title bar, window border, etc
COLORS = {  # from https://www.rapidtables.com/web/color/RGB_Color.html
    "maroon": (128, 0, 0),
    "darkred": (139, 0, 0),
    "brown": (165, 42, 42),
    "firebrick": (178, 34, 34),
    "crimson": (220, 20, 60),
    "red": (255, 0, 0),
    "tomato": (255, 99, 71),
    "coral": (255, 127, 80),
    "indianred": (205, 92, 92),
    "lightcoral": (240, 128, 128),
    "darksalmon": (233, 150, 122),
    "salmon": (250, 128, 114),
    "lightsalmon": (255, 160, 122),
    "orangered": (255, 69, 0),
    "darkorange": (255, 140, 0),
    "orange": (255, 165, 0),
    "gold": (255, 215, 0),
    "darkkhaki": (189, 183, 107),
    "khaki": (240, 230, 140),
    "olive": (128, 128, 0),
    "yellow": (255, 255, 0),
    "yellowgreen": (154, 205, 50),
    "darkolivegreen": (85, 107, 47),
    "olivedrab": (107, 142, 35),
    "lawngreen": (124, 252, 0),
    "chartreuse": (127, 255, 0),
    "greenyellow": (173, 255, 47),
    "darkgreen": (0, 100, 0),
    "green": (0, 128, 0),
    "forestgreen": (34, 139, 34),
    "lime": (0, 255, 0),
    "limegreen": (50, 205, 50),
    "lightgreen": (144, 238, 144),
    "palegreen": (152, 251, 152),
    "darkseagreen": (143, 188, 143),
    "mediumspringgreen": (0, 250, 154),
    "springgreen": (0, 255, 127),
    "seagreen": (46, 139, 87),
    "mediumaquamarine": (102, 205, 170),
    "mediumseagreen": (60, 179, 113),
    "lightseagreen": (32, 178, 170),
    "darkslategray": (47, 79, 79),
    "teal": (0, 128, 128),
    "darkcyan": (0, 139, 139),
    "aqua": (0, 255, 255),
    "cyan": (0, 255, 255),
    "lightcyan": (224, 255, 255),
    "darkturquoise": (0, 206, 209),
    "turquoise": (64, 224, 208),
    "mediumturquoise": (72, 209, 204),
    "paleturquoise": (175, 238, 238),
    "aquamarine": (127, 255, 212),
    "powderblue": (176, 224, 230),
    "cadetblue": (95, 158, 160),
    "steelblue": (70, 130, 180),
    "cornflower blue": (100, 149, 237),
    "deepskyblue": (0, 191, 255),
    "dodgerblue": (30, 144, 255),
    "lightblue": (173, 216, 230),
    "skyblue": (135, 206, 235),
    "lightskyblue": (135, 206, 250),
    "midnightblue": (25, 25, 112),
    "navy": (0, 0, 128),
    "darkblue": (0, 0, 139),
    "mediumblue": (0, 0, 205),
    "blue": (0, 0, 255),
    "royalblue": (65, 105, 225),
    "blueviolet": (138, 43, 226),
    "indigo": (75, 0, 130),
    "darkslateblue": (72, 61, 139),
    "slateblue": (106, 90, 205),
    "mediumslateblue": (123, 104, 238),
    "mediumpurple": (147, 112, 219),
    "darkmagenta": (139, 0, 139),
    "darkviolet": (148, 0, 211),
    "darkorchid": (153, 50, 204),
    "mediumorchid": (186, 85, 211),
    "purple": (128, 0, 128),
    "thistle": (216, 191, 216),
    "plum": (221, 160, 221),
    "violet": (238, 130, 238),
    "magenta": (255, 0, 255),
    "fuchsia": (255, 0, 255),
    "orchid": (218, 112, 214),
    "mediumvioletred": (199, 21, 133),
    "palevioletred": (219, 112, 147),
    "deeppink": (255, 20, 147),
    "hotpink": (255, 105, 180),
    "lightpink": (255, 182, 193),
    "pink": (255, 192, 203),
    "antiquewhite": (250, 235, 215),
    "beige": (245, 245, 220),
    "bisque": (255, 228, 196),
    "blanchedalmond": (255, 235, 205),
    "wheat": (245, 222, 179),
    "cornsilk": (255, 248, 220),
    "lemonchiffon": (255, 250, 205),
    "lightyellow": (255, 255, 224),
    "saddlebrown": (139, 69, 19),
    "sienna": (160, 82, 45),
    "chocolate": (210, 105, 30),
    "peru": (205, 133, 63),
    "sandybrown": (244, 164, 96),
    "burlywood": (222, 184, 135),
    "tan": (210, 180, 140),
    "rosybrown": (188, 143, 143),
    "moccasin": (255, 228, 181),
    "navajowhite": (255, 222, 173),
    "peachpuff": (255, 218, 185),
    "mistyrose": (255, 228, 225),
    "lavenderblush": (255, 240, 245),
    "linen": (250, 240, 230),
    "oldlace": (253, 245, 230),
    "papayawhip": (255, 239, 213),
    "seashell": (255, 245, 238),
    "mintcream": (245, 255, 250),
    "slategray": (112, 128, 144),
    "lightslategray": (119, 136, 153),
    "lightsteelblue": (176, 196, 222),
    "lavender": (230, 230, 250),
    "floralwhite": (255, 250, 240),
    "aliceblue": (240, 248, 255),
    "ghostwhite": (248, 248, 255),
    "honeydew": (240, 255, 240),
    "ivory": (255, 255, 240),
    "azure": (240, 255, 255),
    "snow": (255, 250, 250),
    "black": (0, 0, 0),
    "dimgray": (105, 105, 105),
    "dimgrey": (105, 105, 105),
    "gray": (128, 128, 128),
    "grey": (128, 128, 128),
    "darkgray": (169, 169, 169),
    "darkgrey": (169, 169, 169),
    "silver": (192, 192, 192),
    "lightgray": (211, 211, 211),
    "lightgrey": (211, 211, 211),
    "gainsboro": (220, 220, 220),
    "whitesmoke": (245, 245, 245),
    "white": (255, 255, 255),
}


def get_screen_size():
    info = pg.display.Info()
    return info.current_w, info.current_h


def parse_color(color):
    if isinstance(color, str):
        color = color.lower()
        if color.startswith("#"):
            return tuple(int(color[(1 + 2 * i) : (3 + 2 * i)], 16) for i in range(3))
        elif color in COLORS:
            return COLORS[color]
        else:
            raise Exception(f"Unknown color {color!r}")
    elif isinstance(color, int):
        return tuple((color >> (8 * i)) & 0xFF for i in range(3))
    else:
        raise Exception(f"Unknown color {color!r}")


def average_color(img):
    return np.average(img, axis=(0, 1))


def draw_leaf(surface, leaf, img):
    s = leaf.topleft()
    e = leaf.bottomright()
    sx, sy = s.astype(int)
    ex, ey = e.astype(int)

    color = average_color(img[sx:ex, sy:ey])
    center = ((s + e) / 2).astype(int)
    radii = (leaf.size / 2).astype(int)

    gfx.aaellipse(surface, *center, *radii, color)  # type: ignore
    gfx.filled_ellipse(surface, *center, *radii, color)  # type: ignore


def main(img, background=None, sensibility=1, min_size=1):
    *size, _ = img.shape

    pg.event.set_blocked(None)
    pg.event.set_allowed([pg.QUIT, pg.MOUSEBUTTONDOWN])

    display = pg.display.set_mode(size=size)
    pg.display.set_caption("QuadCircle")
    tree = QuadTree(size, min_size)
    redraw = tree.node

    # fill background once
    if background is None:
        display.fill(average_color(img))

    while True:
        if background is not None:
            display.fill(background)
        if redraw is not None:
            redraw.traverse(lambda leaf: draw_leaf(display, leaf, img))
            pg.display.update(redraw.rect())  # type: ignore

        m = np.array(pg.mouse.get_pos())
        d = pg.mouse.get_rel()
        if np.inner(d, d) >= sensibility ** 2:
            redraw = tree.divide(m)
        else:
            redraw = None

        # events, allow mouse, quit for
        for event in pg.event.get():
            if event.type == pg.QUIT:
                return
            elif event.type == pg.MOUSEBUTTONDOWN and event.button == 1:  # left mouse click
                tree.divide_all()
                redraw = tree.node


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument(
        "-i",
        "--img",
        help="Path to the image to draw, else a default image will be used",
        type=str,
        required=False,
        default=None,
    )
    parser.add_argument(
        "-s",
        "--scale",
        help="The factor to scale the image width",
        type=float,
        required=False,
        default=2,
    )
    parser.add_argument(
        "-e",
        "--sensibility",
        help="How much the mouse has to move to register as a slice (in px)",
        type=float,
        required=False,
        default=1,
    )
    parser.add_argument(
        "-m",
        "--min-size",
        help="Minimum size of a circle",
        type=int,
        required=False,
        default=4,
    )
    parser.add_argument(
        "--bg",
        dest="background",
        help="The background color, 'average' for average image color, 'none' for no background",
        type=str,
        required=False,
        default="average",
    )

    args = parser.parse_args()

    base = getattr(sys, "_MEIPASS", os.getcwd())
    path = join(base, args.img or "imgs/default.jpg")
    img = cv2.imread(path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # opencv bgr -> pygame rgb
    img = np.swapaxes(img, 0, 1)  # opencv height|width -> pygame width|height

    screen_w, screen_h = get_screen_size()
    img_w, img_h, _ = img.shape
    scale = min(args.scale, screen_w / img_w * MAX_SIZE, screen_h / img_h * MAX_SIZE)

    img = cv2.resize(img, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)

    background = args.background.lower()
    if background == "none":
        background = None
    elif background == "average":
        background = average_color(img)
    else:
        background = parse_color(args.background)

    main(img, background=background, sensibility=args.sensibility, min_size=args.min_size)
